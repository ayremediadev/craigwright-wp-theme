<?php get_header('single');?>
<div class="jumbotron jumbotron-fluid" style="background-color: #363636; margin-bottom: 0;">
	<div class="container">
		<div class="row d-flex align-items-center justify-content-between">
			<div class="col-lg-9">
				<h2 class="text-white text-uppercase">Error 404</h2>
				<hr width="100" class="sep">
				<h5 style="color: #fff;">Page not found</h5>
			</div>
			
		</div>
	</div>
</div><div class="container pt-4" >
	<div class="row pb-5 d-flex align-items-center">
		<?php 
		$args = array('orderby' => 'meta_value_num',
			'order' => 'ASC',
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'post_order',
					'compare' => 'NOT EXISTS'
				),
				array(
					'key' => 'post_order',
					'value' => 0,
					'compare' => '>='
				)
			),
			'hide_empty' => true,
			'parent' => 0);
		$categories = get_terms( 'category', $args );
			//print_r($categories);
			$separator = ' | ';
			$output = '';
			if ( ! empty( $categories ) ) {
		    foreach( $categories as $category ) {
				if($category->name != 'Uncategorized'){
					$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '" class="px-2 category-'. $category->term_id .'">' . esc_html( $category->name ) . '</a>' . ' ' .$separator. ' ';
				}
		    } ?>
		    <div class="col-lg-10 allcat">
		    	<a href="http://craigwright.gq/blog/" class="pr-2" style="color: #2555ac; text-decoration: none;">All</a>
				<?php echo '|'.trim( $output, $separator );
			} ?>
			</div>
			<div class="col-lg-2">
				<form id="searchform" class="searchform navbar-form" role="search" method="get" action="<?php echo home_url(); ?>">
					<div class="input-group add-on">
						<input class="form-control" placeholder="Search" name="s" id="s" type="text">
						<div class="input-group-btn">
							<button class="btn btn-default" id="searchsubmit" type="submit"><i class="fas fa-search"></i></button>
						</div>
					</div>
				</form>
			</div>
	</div>
	<div class="row" id="allposts">
		<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
		<?php query_posts( array('orderby'=> 'date','order' => 'DESC','posts_per_page' => 9, 'paged' => $paged));
			while ( have_posts() ) : the_post();
				get_template_part( 'parts/home', 'feed');
			endwhile; ?>
			
<?php 
if (function_exists("bootstrap_pagination"))
{
	bootstrap_pagination();
    //bootstrap_pagination($the_query->max_num_pages);
}
?> 

	</div>
	<?php  //do_shortcode('[ajax_load_more]');?>
</div>
<section id="newsletter-form" class="py-5">
	<?php get_template_part( 'parts/subscribe'); ?>
</section>
<?php get_footer();?>