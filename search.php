<?php get_header();?>
<div class="container" >
	<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
	}
	?>
	<h2 class="text-center" style="margin-bottom: 1.5em;"><?php single_term_title(); ?></h2>
	<div class="row" id="allposts">
		<?php if ( have_posts() ) :
			// Start the Loop.
			$obj = get_queried_object(); 
			query_posts( array('s' => $_GET['s'], 'posts_per_page' => 6, 'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),));
			while ( have_posts() ) : the_post();
				get_template_part( 'parts/home', 'feed');
			endwhile; ?> 
			<div style="width: 100%; padding-bottom: 2em;" class="navigation text-center">
				<div class="alignleft"><?php previous_posts_link('&laquo; Previous') ?></div>
				<div class="alignright"><?php next_posts_link('Next &raquo;') ?></div>
			</div>
		<?php endif; ?>
	</div>
	<?php  //do_shortcode('[ajax_load_more]');?>
</div>
<section id="newsletter-form" class="py-5">
	<?php get_template_part( 'parts/subscribe'); ?>
</section>
<?php get_footer();?>