<?php get_header();?>
<div class="container" >
	<div class="row align-items-start  justify-content-between" id="allposts">

	<div class="text-right col-lg-4 offset-lg-8" style="margin-bottom: 1.5em;">
		<form id="searchform" class="searchform navbar-form" role="search" method="get" action="<?php echo home_url(); ?>">
			<div class="input-group add-on">
				<input class="form-control" placeholder="Search" name="s" id="s" type="text">
				<div class="input-group-btn">
				<button class="btn btn-default" id="searchsubmit" type="submit"><i class="fas fa-search"></i></button>
				</div>
			</div>
		</form>
	</div>
<?php if( get_field('featured_posts') ): ?>
	<?php get_template_part( 'parts/home', 'featured'); ?>
<?php endif; ?>
	<div class="col-lg-12 mb-3">
		<h4 class="text-uppercase txt-color-brown">Latest Posts</h4>
	</div>
	<?php if(is_front_page()){ 
			$featuredquery = get_field('featured_posts', false, false);
			query_posts( array('orderby'=> 'date','order' => 'DESC','posts_per_page' => 6, 'post__not_in' => $featuredquery));
			while ( have_posts() ) : the_post();
				get_template_part( 'parts/home', 'feed');
			endwhile;
			wp_reset_postdata();
		} ?>
	</div>
	<?php  //do_shortcode('[ajax_load_more]');?>
</div>
<section id="newsletter-form" class="py-5">
	<?php get_template_part( 'parts/subscribe'); ?>
</section>
<?php get_footer();?>