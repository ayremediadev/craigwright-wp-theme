<?php get_header();?>
<div class="container" >
	<div class="row" id="allposts">
	<div class="col-lg-8">
		<h4 class="text-uppercase txt-color-brown">Latest Posts</h4>
	</div>
	<div class="text-right col-lg-4" style="margin-bottom: 1.5em;">
		<form id="searchform" class="searchform navbar-form" role="search" method="get" action="<?php echo home_url(); ?>">
			<div class="input-group add-on">
				<input class="form-control" placeholder="Search" name="s" id="s" type="text">
				<div class="input-group-btn">
				<button class="btn btn-default" id="searchsubmit" type="submit"><i class="fas fa-search"></i></button>
				</div>
			</div>
		</form>
	</div>
	<?php if(is_front_page()){ 
			query_posts( array('orderby'=> 'date','order' => 'DESC','posts_per_page' => 6));
			while ( have_posts() ) : the_post();
				get_template_part( 'parts/home', 'feed');
			endwhile;
		} ?>
	</div>
	<?php  //do_shortcode('[ajax_load_more]');?>
</div>
<section id="newsletter-form" class="py-5">
	<?php get_template_part( 'parts/subscribe'); ?>
</section>
<?php get_footer();?>