<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<p>Never miss a story from <span class="txt-color-white">Craig Wright (Bitcoin SV is the original Bitcoin)</span></p>
		</div>
	</div>
	<div class="row">
		<div class="col-2"></div>
		<div class="col-8">
			<?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]');?>
			<?php //echo do_shortcode('[email-subscribers-form id="1"]'); ?>
		</div>
		<div class="col-2"></div>
	</div>
</div>