<?php 
$featured = get_field('featured_posts');
if( $featured ): ?>
    <?php foreach( $featured as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
				<div class="col-12">
					<div class="row align-items-center justify-content-between mx-0 my-3 mb-md-5 mt-md-1 p-0 shadow featuredPosts">
						<div class="col-12 col-md-4 p-0 position-relative overflow-hidden">
							<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>" class="featuredPosts_image d-block position-relative">
								<div class="featuredPosts_label text-uppercase font-weight-bold position-absolute">Featured Post</div>
								<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'main-feature', false ); ?>
								<img src="<?php echo $src[0]; ?>" />
							</a>
						</div>
						<div class="col-12 col-md-8 p-0">
							<div class="row align-items-center justify-content-between m-0 p-3">
								<div class="col-12 my-3 featuredPosts_title"><a href="<?php echo get_permalink( ); ?>" class="text-decoration-none" title="<?php the_title($post->ID); ?>"><h2 class="m-0"><?php the_title(); ?></h2></a></div>
								<div class="col-12 featuredPosts_meta">
									<div class="post-details">
										<span>By <?php $fname = get_the_author_meta('first_name'); $lname = get_the_author_meta('last_name'); echo trim( "$fname $lname" ); ?></span> | <span><?php echo get_the_date('d M Y'); ?></span> | <span class="txt-color-brown"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) { the_category( ', ' ); }?></span>
									</div>
								</div>
								<div class="col-12"><hr width="100" class="sep"></div>
								<div class="col-12 mb-3"><p><?php echo wp_trim_words( get_the_content($post->ID), 70 ); ?></p></div>
								<div class="col-12 mb-3"><a href="<?php echo get_permalink($post->ID); ?>" class="btn btn-light rounded-pill px-4 text-uppercase">Read More</a></div>
							</div>
						</div>
					</div>
				</div>
	 <?php endforeach; ?>
	 <?php wp_reset_postdata(); ?>
<?php endif; ?>