<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'archive-image' ); 
//if($url){ ?>
	<div class="col-lg-4 mb-5 post-card">
		<a href="<?php echo get_permalink( $post->ID ); ?>"><div class="col-lg-12 pb-12 post-card-image" style="height: 150px; background: url(<?php echo $url; ?>); background-size: cover; background-position: center;">
			
		</div></a>
		<!---<a href="<?php //echo get_permalink( $post->ID ); ?>"><img src="<?php //echo $url ?>" /></a>--->
		<a href="<?php //echo get_permalink( $post->ID ); ?>"><div class="px-4 pt-4 pb-4 box-shadow card-details">
			<?php the_title( sprintf( '<h3 class="entry-title card-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );?>
			<div class="post-details">
				<span>By <?php $fname = get_the_author_meta('first_name'); $lname = get_the_author_meta('last_name'); echo trim( "$fname $lname" ); ?></span> | <span><?php echo get_the_date('d M Y'); ?></span> | <span class="txt-color-brown"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) { the_category( ', ' ); }?></span>
			</div>
			<hr width="100" class="sep">
			<div class="entry-content">
				<?php the_excerpt(); ?>
			</div>
			<a href="<?php echo get_permalink( $post->ID ); ?>" class="btn btn-primary btn-lg">Read more</a>
		</div></a>
	</div>		
<?php  //} ?>