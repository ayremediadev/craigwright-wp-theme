<?php 
	
add_theme_support( 'custom-header' );
add_theme_support( 'post-thumbnails' );

//CUSTOM IMAGE SIZE
add_image_size( 'main-feature', 370,420, array( 'center', 'center' ) );
add_image_size( 'category-feature', 365,476, array( 'center', 'center' ) );
add_image_size( 'archive-image', 350,150, array( 'center', 'center' ) );

register_sidebar( array(
	'name' => 'Footer Sidebar 1',
	'id' => 'footer-sidebar-1',
	'description' => 'Appears in the footer area',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );

register_sidebar( array(
	'name' => 'Aside Sidebar 1', 
	'id' => 'aside-sidebar-1', 
	'description' => 'Appears in the right hand-side area', 
	'before_widget' => '<aside id="%1$s" class="widget %2$s">', 
	'after_widget' => '</aside>', 
	'before_title' => '<h3 class="widget-title">', 
	'after_title' => '</h3>', 
) );

function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function cw_script() {
    wp_enqueue_script( 'custom', get_template_directory_uri() . '/custom.js', array(), '1.0.0' );
}
add_action( 'wp_enqueue_scripts', 'cw_script' );
	
function part($i){
	include(get_template_directory() . '/parts/' . $i . '.php' );
}

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function theme_prefix_setup() {
	
	add_theme_support( 'custom-logo', array(
		'flex-width' => true,
	) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );

function theme_prefix_the_custom_logo() {
	
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}

}

add_action('wpcf7_before_send_mail', 'contact_form_to_subs');
function contact_form_to_subs($wpcf7)
{
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    global $wpdb;
    //$email=$wpcf7->posted_data["your-email"];
	$email = $_POST['your-email'];
    $wpdb->insert( $wpdb->prefix . 'wppen_subscribers', 
            array(
			'email' => $email, 
			'ip' => $ip, 
			'created_gmt' => date('Y-m-d H:i:s')));
}


function bootstrap_pagination($pages = '', $range = 2) 
{  
	$showitems = ($range * 2) + 1;  
	global $paged;
	if(empty($paged)) $paged = 1;
	if($pages == '')
	{
		global $wp_query; 
		$pages = $wp_query->max_num_pages;
	
		if(!$pages)
			$pages = 1;		 
	}   
	
	if(1 != $pages)
	{
		echo '<div class="container-fluid">';
		echo '<div class="row align-items-center my-4 justify-content-center">';
		echo '<div class="col-12">';
	    echo '<nav aria-label="Page navigation" role="navigation">';
        echo '<span class="sr-only">Page navigation</span>';
        echo '<ul class="pagination justify-content-center ft-wpbs">';
		
        //echo '<li class="page-item disabled hidden-md-down d-none d-lg-block"><span class="page-link">Page '.$paged.' of '.$pages.'</span></li>';
	
	 	if($paged > 2 && $paged > $range+1 && $showitems < $pages) 
			echo '<li class="page-item"><a class="page-link" href="'.get_pagenum_link(1).'" aria-label="First Page"><span aria-hidden="true">&laquo;</span></a></li>';
	
	 	if($paged > 1 && $showitems < $pages) 
			echo '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($paged - 1).'" aria-label="Previous Page">&lsaquo;</a></li>';
	
		for ($i=1; $i <= $pages; $i++)
		{
		    if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
				echo ($paged == $i)? '<li class="page-item active"><span class="page-link"><span class="sr-only">Current Page </span>'.$i.'</span></li>' : '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($i).'"><span class="sr-only">Page </span>'.$i.'</a></li>';
		}
		
		if ($paged < $pages && $showitems < $pages) 
			echo '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($paged + 1).'" aria-label="Next Page">&rsaquo;</a></li>';  
	
	 	if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) 
			echo '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($pages).'" aria-label="Last Page"><span aria-hidden="true">&raquo;</span></a></li>';
	
	 	echo '</ul>';
		echo '</nav>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
        //echo '<div class="pagination-info mb-5 text-center">[ <span class="text-muted">Page</span> '.$paged.' <span class="text-muted">of</span> '.$pages.' ]</div>';	 	
	}
}

function breadcrumb(){
	if(is_single()){
		$i = get_queried_object_id(); 
		$t = get_term_by('term_id', $i, 'category');
		$c =  get_post_primary_category($i, 'category');

		echo '<p><a href="' . home_url() . '/blog/" style="color: #000; text-decoration: none;">Blog</a> > ' . '<a href="'. get_category_link($c['primary_category']->term_id). '" style="color: #2555ac; text-decoration: none;">' .$c['primary_category']->name  . '</a></p>';
	}
}

function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
   $return = array();

   if (class_exists('WPSEO_Primary_Term')){
       // Show Primary category by Yoast if it is enabled & set
       $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
       $primary_term = get_term($wpseo_primary_term->get_primary_term());

       if (!is_wp_error($primary_term)){
           $return['primary_category'] = $primary_term;
       }
   }

   if (empty($return['primary_category']) || $return_all_categories){
       $categories_list = get_the_terms($post_id, $term);

       if (empty($return['primary_category']) && !empty($categories_list)){
           $return['primary_category'] = $categories_list[0];  //get the first category
       }
       if ($return_all_categories){
           $return['all_categories'] = array();

           if (!empty($categories_list)){
               foreach($categories_list as &$category){
                   $return['all_categories'][] = $category->term_id;
               }
           }
       }
   }

   return $return;
}