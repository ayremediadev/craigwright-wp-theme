<!DOCTYPE html>
<?php 
global $wp;
$u = home_url( add_query_arg( array(), $wp->request ) );
$t = get_the_title(); 
$i = wp_get_attachment_url( get_post_thumbnail_id($wp->ID) );
$cat = get_the_category();
 if ( ! empty( $categories ) ) {
   $d = esc_html( $categories[0]->name );   
}
?>
<html <?php language_attributes(); ?>>
<head>
	<title><?php echo 'Craig Wright - ' . $t; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:url" content="<?php echo $u; ?>">
	<meta property="og:type" content="article">
	<meta property="og:title" content="<?php echo $t;?>">
	<meta property="og:description" content="<?php  echo $d; ?>">
	<meta property="og:image" content="<?php echo $i; ?>">

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/custom.js'"></script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5K9GQWR');</script>
	<!-- End Google Tag Manager -->

	<!--Theme styles -->
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>"/>
	<!--Font Awesome free version -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<script>
		jQuery(function(){
			$('#menu-main').addClass('list-inline');
			$('#menu-main li').addClass('list-inline-item');
		});
	</script>
	
</head>
<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5K9GQWR"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
<header >
	<div id="site-header-single" class="jumbotron py-0" style="background-color: #141414; margin-bottom: 0;">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-12 col-md my-2">
					<a href="<?php echo site_url(); ?>"><img src="http://craigwright.net/wp-content/uploads/2019/05/Logo-Section.png" class="img-fluid" /></a>
				</div>
				<div class="col-12 col-md-auto text-center">
					<nav>
					<?php wp_nav_menu(); ?>
					</nav>
				</div>
			</div>
		</div>
	</div>
</header>