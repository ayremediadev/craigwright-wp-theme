<?php get_header('single');?>
<div class="jumbotron jumbotron-fluid" style="background-color: #363636; margin-bottom: 0;">
	<div class="container">
		<div class="row d-flex align-items-center justify-content-between">
			<div class="col-lg-9">
				<h2 class="text-white text-uppercase"><?php single_term_title(); ?></h2>
				<hr width="100" class="sep">
				<?php if(get_field('header_caption')){ 
					echo '<h5 style="color: #fff;">' . get_field('header_caption') . '</h5>';
				} ?>
			</div>
			
		</div>
	</div>
</div>
<div class="container pt-4">
	<div class="row pb-5 d-flex align-items-center">
		<?php 
		$args = array('orderby' => 'meta_value_num',
			'order' => 'ASC',
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'post_order',
					'compare' => 'NOT EXISTS'
				),
				array(
					'key' => 'post_order',
					'value' => 0,
					'compare' => '>='
				)
			),
			'hide_empty' => true,
			'parent' => 0);
			$categories = get_terms( 'category', $args);
			//print_r($categories);
			$separator = ' | ';
			$output = '';
			$queried_object = get_queried_object();
			if ( ! empty( $categories ) ) {
		    foreach( $categories as $category ) {
				if($category->name != 'Uncategorized'){
					if($category->slug == $queried_object->slug){
						$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" style="color: #2555ac; text-decoration: none;" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '" class="px-2">' . esc_html( $category->name ) . '</a>' . ' ' .$separator. ' ';
					} else {
						$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" style="text-decoration: none;" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '" class="px-2">' . esc_html( $category->name ) . '</a>' . ' ' .$separator. ' ';
					}
				}
		    } ?>
		    <div class="col-lg-10 allcat">
		    	<a href="http://craigwright.net/blog/" class="pr-2">All</a>
		    <?php echo '|'.trim( $output, $separator );
			} ?>
			</div>
			<div class="col-lg-2">
				<form id="searchform" class="searchform navbar-form" role="search" method="get" action="<?php echo home_url(); ?>">
					<div class="input-group add-on">
						<input class="form-control" placeholder="Search" name="s" id="s" type="text">
						<div class="input-group-btn">
							<button class="btn btn-default" id="searchsubmit" type="submit"><i class="fas fa-search"></i></button>
						</div>
					</div>
				</form>
			</div>
	</div>
	<div class="row TT" id="allposts">

<?php 
// the query to set the posts per page to 3
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$id = get_queried_object_id(); 
$term = get_term($id, 'category');
$slug = $term->slug;
$args = array('posts_per_page' => 12, 'paged' => $paged, 'category_name' => $slug );
query_posts($args); ?>
<!-- the loop -->
<?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
	<?php get_template_part( 'parts/home', 'feed'); ?>
<?php endwhile; ?>
<!-- pagination -->
<?php 
if (function_exists("bootstrap_pagination"))
{
	bootstrap_pagination();
    //bootstrap_pagination($the_query->max_num_pages);
}
?> 
<?php else : ?>
<!-- No posts found -->
<?php endif; ?>

	</div>
	<?php  //do_shortcode('[ajax_load_more]');?>
</div>
<section id="newsletter-form" class="py-5">
	<?php get_template_part( 'parts/subscribe'); ?>
</section>
<?php get_footer();?>