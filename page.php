<?php
get_header('single');?>

<div class="jumbotron jumbotron-fluid" style="background-color: #363636; margin-bottom: 0;">
	<div class="container">
		<div class="row d-flex align-items-center justify-content-between">
			<div class="col-lg-9">
				<h1 class="text-white text-uppercase"><?php the_title(); ?></h1>
				<hr width="100" class="sep">
				<?php if(get_field('header_caption')){ 
					echo '<h5 style="color: #fff;">' . get_field('header_caption') . '</h5>';
				} ?>
			</div>
			
		</div>
	</div>
</div>
<div class="container py-5" >

	<div class="row py-5 d-flex align-items-center">
		<?php $img = get_field('img');
			  $n = get_field('info_text');
			  if($img && $n){ ?>
			  	<div class="col-lg-3 pr-0 block-img">
			  		<img src="<?php echo $img; ?>" style="width:100%;">
			  	</div>
			  	<div class="col-lg-4 py-2 solid-bg-blue text-white">
			  		<?php echo $n; ?>
			  	</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="col-lg-12">
<?php if (have_posts()) : while (have_posts()) : the_post(); 
 		the_content(); 
	endwhile; endif; ?>
		 </div>
	</div>

	<?php  //do_shortcode('[ajax_load_more]');?>
</div>
<section id="newsletter-form" class="py-5">
	<?php get_template_part( 'parts/subscribe'); ?>
</section>
<?php get_footer();?>