<!DOCTYPE html>
<?php
$t = get_option('blogname'); 
?>
<html <?php language_attributes(); ?>>
<head>
	
	<title><?php echo $t; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo get_site_icon_url();?>" type="image/x-icon">
	<link rel="icon" href="<?php echo get_site_icon_url();?>" type="image/x-icon">
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5K9GQWR');</script>
	<!-- End Google Tag Manager -->
	
	<!-- FontAwesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	
	<!-- Theme styles -->
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>"/>

	<script>
		jQuery(function(){
			$('#menu-main').addClass('list-inline');
			$('#menu-main li').addClass('list-inline-item');
		});
	</script>
	
</head>
<body <?php body_class(); ?> >
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5K9GQWR"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	
<header style="margin-bottom: 2em;">
	<div class="topmenu">
		<div class="container">
			<div class="row align-items-center justify-content-end">
				<div class="col-12 col-md-auto text-center">
					<nav>
					<?php wp_nav_menu(); ?>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<?php 
		if ( get_header_image() ) : ?>
		<div id="site-header" class="jumbotron" style="background-repeat: no-repeat; background-image: url(<?php header_image(); ?>); background-position: top center; background-size: cover; margin-bottom: 0;">
			<div class="container text-center">
				<a href="<?php echo home_url(); ?>" class="navbar-brand"> 
					<?php 
					$custom_logo_id = get_theme_mod( 'custom_logo' );
					$custom_logo_url = wp_get_attachment_image_url( $custom_logo_id , 'full' );
					echo '<img src="' . esc_url( $custom_logo_url ) . '" alt="Craig Wright"></a>';?>
				</a>
				<p class="tagline text-uppercase pt-2"><?php echo get_option('blogdescription'); ?></p>
				<hr class="my-5 sep-header">
				<div class="col-lg-8 text-center ml-auto mr-auto">
					<p class="txt-color-white mt-2">
						<?php if(get_field('header_caption')){
							echo get_field('header_caption');
						} ?>
					</p>
				</div>

			</div>
		</div>
	<?php endif; ?>
</header>