<?php

/***

Template Name: About Craig

***/

get_header('single');?>
<script type="text/javascript">
$(document).ready(function() {
        
   /* activate the carousel */
   $("#modal-carousel").carousel({interval:false});

   /* change modal title when slide changes */
   $("#modal-carousel").on("slid.bs.carousel",       function () {
        $(".modal-title")
        .html($(this)
        .find(".active img")
        .attr("title"));
   });

   /* when clicking a thumbnail */
   $(".imgcard").click(function(){
    var content = $(".carousel-inner");
    var title = $(".modal-title");
  
    content.empty();  
    title.empty();
  
  	var id = this.id;  
     var repo = $("#img-repo .carousel-item");
     var repoCopy = repo.filter("#" + id).clone();
     var active = repoCopy.first();
  
    active.addClass("active");
    title.html(active.find("img").attr("title"));
  	content.append(repoCopy);

    // show the modal
  	$("#modal-gallery").modal("show");
  });

});
</script>

<style type="text/css">

.carousel-control.left,.carousel-control.right{
  background-image:none;
  margin-top:10%;
  width:5%;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
</style>
<div class="jumbotron jumbotron-fluid" style="background-color: #363636; margin-bottom: 0;">
	<div class="container">
		<div class="row d-flex align-items-center justify-content-between">
			<div class="col-lg-9">
				<h1 class="text-white text-uppercase"><?php the_title(); ?></h1>
				<hr width="100" class="sep">
				<?php if(get_field('header_caption')){ 
					echo '<h5 style="color: #fff;">' . get_field('header_caption') . '</h5>';
				} ?>
			</div>
			
		</div>
	</div>
</div>
<div class="container py-5" >

	<div class="row">
		<div class="col-lg-12">
			<!---<ul class="d-inline-block px-0 nav menu-about" id="ab-tab">
				<li class="d-inline-block "><a class="text-dark active" href="#biography-content" data-toggle="tab" id="biolink">Biography</a> <span class="px-3">|</span></li>
				<li class="d-inline-block"><a class="text-dark" href="#degree-content" data-toggle="tab" id="deglink">Academic Degrees</a> <span class="px-3">|</span></li>
				<li class="d-inline-block"><a class="text-dark" href="#certificate-content" data-toggle="tab" id="certlink">Professional Certifications</a></li>
			</ul>
		</div>--->
	</div>
	<div class="tab-content">
	<div id="biography-content" class="tab-pane active">
		<div class="row py-5 d-flex align-items-center">
			<?php $img = get_field('img');
				  $n = get_field('info_text');
				  if($img && $n){ ?>
				  	<div class="col-lg-3 pr-0 block-img">
				  		<img src="<?php echo $img; ?>" style="width:100%;">
				  	</div>
				  	<div class="col-lg-4 py-2 solid-bg-blue text-white">
				  		<?php echo $n; ?>
				  	</div>
			<?php } ?>
		</div>
		<div class="row">
			<div class="col-lg-12">
			<?php if (have_posts()) : while (have_posts()) : the_post(); 
	 				the_content(); 
				endwhile; endif; ?>
			</div>
		</div>
	</div>
	
		<?php

		// check if the repeater field has rows of data
		if( have_rows('certificate') ): ?>

			<div id="degree-content" class="tab-pane">
				<div class="row my-4">
		<?php  $i = 0;  
			   
			while ( have_rows('certificate') ) : the_row(); 
				$i +=1;
				if( get_sub_field('type') == 'Academic Degrees' ):?>
					<div class="col-lg-4 degree-card py-4 text-center">
						<div class="card-title d-flex align-items-center justify-content-center">
							<h4 class="py-2"><?php the_sub_field('title'); ?></h4>
						</div>
	        			
	        			<?php  
	        				$rows = get_sub_field('image_block');// get all the rows
							$first_row = $rows[0]; // get the first row
							$first_row_image = $first_row['image_url' ]; // get the sub field value 
							$count_r = count(get_sub_field('image_block'));
							$im = 'singleimg';
							if($count_r>1){
								$im = 'multipleimg';
							} ?>

							<a href="#"><img src="<?php echo $first_row_image; ?>" class="imgcard img-thumbnail <?php echo $im;?> " id="image-<?php echo $i; ?>"></a>
							
							<div class="d-none" id="img-repo">
							  
							<?php while ( have_rows('image_block') ) : the_row(); ?>

							    <div class="carousel-item" id="image-<?php echo $i; ?>">
							      <img src="<?php the_sub_field('image_url') ?>" class="img-fluid <?php echo $arr; ?>">
							    </div>
					    	
					    	<?php endwhile;?>
  
							</div>		
			        </div>
	<?php		endif; ?>
	<?php   endwhile; 
			//echo $arr;?>
				</div>
			</div>
				<div id="certificate-content" class="tab-pane">
				<div class="row my-4">
		<?php  $i = 0;  
			while ( have_rows('certificate') ) : the_row(); 
				$i +=1;
				if( get_sub_field('type') == 'Professional Certifications' ):?>
					<div class="col-lg-4 degree-card py-4 text-center">
	        			<div class="card-title d-flex align-items-center justify-content-center">
							<h4 class="py-2"><?php the_sub_field('title'); ?></h4>
						</div>
	        			<?php  
	        				$rows = get_sub_field('image_block');// get all the rows
							$first_row = $rows[0]; // get the first row
							$first_row_image = $first_row['image_url' ]; // get the sub field value
							$count_r = count(get_sub_field('image_block'));
							$im = 'singleimg';
							if($count_r>1){
								$im = 'multipleimg';
							} ?>

							<a href="#"><img src="<?php echo $first_row_image; ?>" class="imgcard img-thumbnail  <?php echo $im;?> " id="image-<?php echo $i; ?>"></a>
							
							<div class="d-none" id="img-repo">
							  
							<?php while ( have_rows('image_block') ) : the_row(); ?>

							    <div class="carousel-item" id="image-<?php echo $i; ?>">
							      <img src="<?php the_sub_field('image_url') ?>" class="img-fluid">
							    </div>
					    	
					    	<?php endwhile;?>
  
							</div>		
			        </div>
	<?php		endif; ?>
	<?php   endwhile; ?>
					</div>
				</div>
<?php	endif; ?>
</div>
<div class="modal" id="modal-gallery" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button class="close" type="button" data-dismiss="modal">×</button>
          <h3 class="modal-title"></h3>
      </div>
      <div class="modal-body">
          <div id="modal-carousel" class="carousel slide" data-ride="carousel">
   
            <div class="carousel-inner">           
            </div>
        <a class="carousel-control-prev " href="#modal-carousel" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"><i class="fa fa-chevron-left"></i></span>
		    <span class="sr-only">Previous</span>
		</a>
  <a class="carousel-control-next" href="#modal-carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"><i class="fa fa-chevron-right"></i></span>
    <span class="sr-only">Next</span>
  </a>
          </div>
      </div>
<!--       <div class="modal-footer">
      </div> -->
    </div>
  </div>
</div>
</div>
<section id="newsletter-form" class="py-5">
	<?php get_template_part( 'parts/subscribe'); ?>
</section>
<?php get_footer();?>