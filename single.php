<?php 
	
get_header('single');

?>
<div class="container">
	<div class="row">
		<div class="col-lg-9 pt-4">

			<main>
				<?php while ( have_posts() ) : ?>
					<?php
						breadcrumb();
					?>
					<div class="feat-img-post mb-5 text-center">
						<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
						<img src="<?php echo $url ?>" />
					</div>
					
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php the_post(); ?>
					<div class="post-details">
						<span>By <?php $fname = get_the_author_meta('first_name'); $lname = get_the_author_meta('last_name'); echo trim( "$fname $lname" ); ?></span> | <span><?php the_date('d M Y'); ?></span> | <span class="txt-color-brown"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) { echo esc_html( $categories[0]->name );}?></span>
					</div>
					<hr width="100" class="sep">
						<?php the_content(); ?>
						<hr>
						<?php 
						//tags
						$post_tags = get_the_tags();
 
						if ( $post_tags ) { ?>
							<ul class="tags-list">
						    <?php foreach( $post_tags as $tag ) { ?>
						    		<li>
						    			<?php echo $tag->name; ?>
						    		</li>
	
						   <?php } ?>
							</ul>
						<?php }?>

						<div class="social bg-brown px-5 py-5 d-flex align-items-center">
							<p class="txt-color-white mb-0 f-semi-bold">Did you like this post? Share it:</p>
							<ul class="d-flex align-items-center justify-content-center mb-0">
								<?php $p = get_the_permalink(); 
									$t = get_the_title(); ?>
								<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $p; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="http://twitter.com/share?text=<?php echo $t; ?>&url=<?php echo $p; ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
								<li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $p; ?>&title=<?php echo $t; ?>&source=LinkedIn" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
						<hr>
					<?php endwhile; ?>
			</main>
		</div>
		<div class="col-lg-3 sidebar-post py-5 mb-5">
			<?php
				if(is_active_sidebar('aside-sidebar-1')){
					dynamic_sidebar('aside-sidebar-1');
				}
			?>
		</div>
	</div>
</div>
<?php
	
get_footer();

?>