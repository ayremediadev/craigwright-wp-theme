	<footer>
		<div class="container">
			<div class="row">
				<div class="col-4 col-xs-0 col-sm-0"></div>
				<div class="col-lg-4 col-md-4 col-sm-12 py-4">
					<?php
					if(is_active_sidebar('footer-sidebar-1')){
						dynamic_sidebar('footer-sidebar-1');
					}
					?>
				</div>
				<div class="col-4 col-xs-0 col-sm-0"></div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</body>