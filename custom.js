jQuery( document ).ready(function($) {

	if (document.location.pathname == '/about/'){
		
		$( ".singleimg" ).click(function() {
		  
			$('.carousel-control-prev').hide();
			$('.carousel-control-next').hide();
		
		});
		$( ".multipleimg" ).click(function() {
		  
			$('.carousel-control-prev').show();
			$('.carousel-control-next').show();
		
		});
		$('#deg-btn').click(function(){
			//alert('degrees');
			$('#biolink').removeClass('active');
			$('#deglink').addClass('active');
			$('#biography-content').removeClass('active');
			$('#degree-content').addClass('active');
			$(window).scrollTop(0);
		});
		$('#cert-btn').click(function(){
			$('#biolink').removeClass('active');
			$('#certlink').addClass('active');
			$('#biography-content').removeClass('active');
			$('#certificate-content').addClass('active');
			$(window).scrollTop(0);
		});

	}
 
});